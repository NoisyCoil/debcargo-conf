From 0ddced447aa4f98883d354c5c52324ef7b770067 Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?Fabian=20Gr=C3=BCnbichler?= <f.gruenbichler@proxmox.com>
Date: Thu, 11 Jul 2024 16:50:03 +0200
Subject: [PATCH] update to prost 0.12
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

derived Enumerations now provide their own conversion implementation
from i32, so map any DecodeErrors originating from there to
InvalidEncoding, and drop the manual impls that existed before.

Closes: #153

Signed-off-by: Fabian Grünbichler <f.gruenbichler@proxmox.com>
---
 Cargo.toml                                   |   4 +-
 src/operations_protobuf/generated_ops/mod.rs | 121 +++----------------
 src/requests/response_status.rs              |  10 ++
 3 files changed, 30 insertions(+), 105 deletions(-)

https://github.com/parallaxsecond/parsec-interface-rs/pull/154/

Index: parsec-interface/Cargo.toml
===================================================================
--- parsec-interface.orig/Cargo.toml
+++ parsec-interface/Cargo.toml
@@ -50,7 +50,7 @@ version = "0.3.0"
 version = "0.2.12"
 
 [dependencies.prost]
-version = "0.11.0"
+version = "0.12.0"
 
 [dependencies.psa-crypto]
 version = "0.9.0"
@@ -72,7 +72,7 @@ version = "1.1.0"
 features = ["zeroize_derive"]
 
 [build-dependencies.prost-build]
-version = "0.11.0"
+version = "0.12.0"
 optional = true
 
 [features]
Index: parsec-interface/src/operations_protobuf/generated_ops/mod.rs
===================================================================
--- parsec-interface.orig/src/operations_protobuf/generated_ops/mod.rs
+++ parsec-interface/src/operations_protobuf/generated_ops/mod.rs
@@ -36,104 +36,15 @@ pub mod prepare_key_attestation;
 
 use zeroize::Zeroize;
 
-use crate::requests::{ResponseStatus, Result};
-use log::error;
-use psa_algorithm::algorithm::{aead::AeadWithDefaultLengthTag, key_agreement::Raw, Cipher, Hash};
-use psa_key_attributes::key_type::{DhFamily, EccFamily};
-use can_do_crypto::CheckType;
+#[cfg(test)]
+use crate::requests::ResponseStatus;
+#[cfg(test)]
+use psa_algorithm::algorithm::{Cipher, Hash};
+#[cfg(test)]
+use psa_key_attributes::key_type::EccFamily;
+#[cfg(test)]
 use std::convert::TryFrom;
 
-impl TryFrom<i32> for Cipher {
-    type Error = ResponseStatus;
-    fn try_from(cipher_val: i32) -> Result<Self> {
-        Cipher::from_i32(cipher_val).ok_or_else(|| {
-            error!(
-                "Value {} not supported as a cipher algorithm encoding.",
-                cipher_val
-            );
-            ResponseStatus::InvalidEncoding
-        })
-    }
-}
-
-impl TryFrom<i32> for Hash {
-    type Error = ResponseStatus;
-    fn try_from(hash_val: i32) -> Result<Self> {
-        Hash::from_i32(hash_val).ok_or_else(|| {
-            error!(
-                "Value {} not supported as a hash algorithm encoding.",
-                hash_val
-            );
-            ResponseStatus::InvalidEncoding
-        })
-    }
-}
-
-impl TryFrom<i32> for AeadWithDefaultLengthTag {
-    type Error = ResponseStatus;
-    fn try_from(aead_val: i32) -> Result<Self> {
-        AeadWithDefaultLengthTag::from_i32(aead_val).ok_or_else(|| {
-            error!(
-                "Value {} not supported as an AEAD with default tag length algorithm encoding.",
-                aead_val
-            );
-            ResponseStatus::InvalidEncoding
-        })
-    }
-}
-
-impl TryFrom<i32> for Raw {
-    type Error = ResponseStatus;
-    fn try_from(key_agreement_val: i32) -> Result<Self> {
-        Raw::from_i32(key_agreement_val).ok_or_else(|| {
-            error!(
-                "Value {} not supported as a raw key agreement algorithm encoding.",
-                key_agreement_val
-            );
-            ResponseStatus::InvalidEncoding
-        })
-    }
-}
-
-impl TryFrom<i32> for EccFamily {
-    type Error = ResponseStatus;
-    fn try_from(ecc_family_val: i32) -> Result<Self> {
-        EccFamily::from_i32(ecc_family_val).ok_or_else(|| {
-            error!(
-                "Value {} not supported as an ECC family encoding.",
-                ecc_family_val
-            );
-            ResponseStatus::InvalidEncoding
-        })
-    }
-}
-
-impl TryFrom<i32> for DhFamily {
-    type Error = ResponseStatus;
-    fn try_from(dh_family_val: i32) -> Result<Self> {
-        DhFamily::from_i32(dh_family_val).ok_or_else(|| {
-            error!(
-                "Value {} not supported as a DH family encoding.",
-                dh_family_val
-            );
-            ResponseStatus::InvalidEncoding
-        })
-    }
-}
-
-impl TryFrom<i32> for CheckType {
-    type Error = ResponseStatus;
-    fn try_from(check_type_val: i32) -> Result<Self> {
-        CheckType::from_i32(check_type_val).ok_or_else(|| {
-            error!(
-                "Value {} not supported as a check type.",
-                check_type_val
-            );
-            ResponseStatus::InvalidEncoding
-        })
-    }
-}
-
 pub(super) trait ClearProtoMessage {
     fn clear_message(&mut self) {}
 }
@@ -299,11 +210,15 @@ impl ClearProtoMessage for psa_cipher_de
 }
 
 impl ClearProtoMessage for psa_cipher_encrypt::Result {
-    fn clear_message(&mut self) { self.ciphertext.zeroize(); }
+    fn clear_message(&mut self) {
+        self.ciphertext.zeroize();
+    }
 }
 
 impl ClearProtoMessage for psa_cipher_decrypt::Result {
-    fn clear_message(&mut self) { self.plaintext.zeroize(); }
+    fn clear_message(&mut self) {
+        self.plaintext.zeroize();
+    }
 }
 
 impl ClearProtoMessage for psa_generate_random::Result {
@@ -402,23 +317,23 @@ impl ClearProtoMessage for prepare_key_a
 #[test]
 fn i32_conversions() {
     assert_eq!(
-        Cipher::try_from(56).unwrap_err(),
+        <::prost::DecodeError as Into<ResponseStatus>>::into(Cipher::try_from(56).unwrap_err()),
         ResponseStatus::InvalidEncoding
     );
     assert_eq!(
-        Cipher::try_from(-5).unwrap_err(),
+        <::prost::DecodeError as Into<ResponseStatus>>::into(Cipher::try_from(-5).unwrap_err()),
         ResponseStatus::InvalidEncoding
     );
     assert_eq!(
-        Hash::try_from(89).unwrap_err(),
+        <::prost::DecodeError as Into<ResponseStatus>>::into(Hash::try_from(89).unwrap_err()),
         ResponseStatus::InvalidEncoding
     );
     assert_eq!(
-        Hash::try_from(-4).unwrap_err(),
+        <::prost::DecodeError as Into<ResponseStatus>>::into(Hash::try_from(-4).unwrap_err()),
         ResponseStatus::InvalidEncoding
     );
     assert_eq!(
-        EccFamily::try_from(78).unwrap_err(),
+        <::prost::DecodeError as Into<ResponseStatus>>::into(EccFamily::try_from(78).unwrap_err()),
         ResponseStatus::InvalidEncoding
     );
 }
Index: parsec-interface/src/requests/response_status.rs
===================================================================
--- parsec-interface.orig/src/requests/response_status.rs
+++ parsec-interface/src/requests/response_status.rs
@@ -295,6 +295,16 @@ impl From<std::io::Error> for ResponseSt
     }
 }
 
+impl From<::prost::DecodeError> for ResponseStatus {
+    fn from(err: ::prost::DecodeError) -> Self {
+        warn!(
+            "Conversion from {} to ResponseStatus::InvalidEncoding.",
+            err
+        );
+        ResponseStatus::InvalidEncoding
+    }
+}
+
 impl From<bincode::Error> for ResponseStatus {
     fn from(err: bincode::Error) -> Self {
         warn!(
