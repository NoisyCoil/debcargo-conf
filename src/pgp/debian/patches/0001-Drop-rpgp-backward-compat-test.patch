From df025e3a9ccc1a45844d312abf8f024bda446b46 Mon Sep 17 00:00:00 2001
From: Daniel Kahn Gillmor <dkg@fifthhorseman.net>
Date: Fri, 30 Aug 2024 00:16:25 -0400
Subject: [PATCH 1/5] Drop rpgp backward-compat test

This test is for dealing with ecdh padding -- older versions of rpgp
couldn't handle proper ecdh padding, so this tries to confirm that
rpgp doesn't generate padding that would be incompatible with those
old versions.

However, it requires an entirely different copy of (a different
version of) rpgp in order to be able to do the test.

For debian's purposes, we're fine without this test.
---
 Cargo.toml               |   4 --
 tests/backward-compat.rs | 120 ---------------------------------------
 2 files changed, 124 deletions(-)
 delete mode 100644 tests/backward-compat.rs

diff --git a/Cargo.toml b/Cargo.toml
index 7d767f1..426337f 100644
--- a/Cargo.toml
+++ b/Cargo.toml
@@ -298,10 +298,6 @@ version = "0.3"
 [dev-dependencies.regex]
 version = "^1.7"
 
-[dev-dependencies.rpgp_0_10]
-version = "=0.10.2"
-package = "pgp"
-
 [dev-dependencies.serde]
 version = "^1.0"
 features = ["derive"]
diff --git a/tests/backward-compat.rs b/tests/backward-compat.rs
deleted file mode 100644
index 16b527a..0000000
--- a/tests/backward-compat.rs
+++ /dev/null
@@ -1,120 +0,0 @@
-use rand::SeedableRng;
-use rand_chacha::ChaChaRng;
-
-// Tests that check for backward compatibility with older versions of rpgp
-
-#[test]
-fn ecdh_roundtrip_with_rpgp_0_10() {
-    // Encrypt/decrypt roundtrip to validate that there is no padding breakage between rPGP versions.
-
-    // Context: rPGP versions before 0.11 couldn't handle "long padding" (that exceeds one block),
-    // see https://github.com/rpgp/rpgp/pull/280
-
-    // However, rPGP 0.12 - 0.13.1 emit "long padding" by default (see https://github.com/rpgp/rpgp/pull/307),
-    // which older rPGP cannot unpad (and thus not decrypt).
-
-    // To avoid incompatibility with the (erroneous) ecdh handling in rPGP <0.11, rPGP produces
-    // "short padding" again, starting with 0.13.2
-
-    // Note: We use AES128 in this test so that the encrypting party is able to use "long padding".
-
-    const MSG: &[u8] = b"hello world";
-
-    // a test-key with an ECDH(Curve25519) encryption subkey
-    const KEYFILE: &str = "./tests/unit-tests/padding/alice.key";
-
-    // 0.10 -> 0.10
-    let enc = encrypt_rpgp_0_10(MSG, KEYFILE);
-    let dec = decrypt_rpgp_0_10(&enc, KEYFILE);
-    assert_eq!(dec, MSG, "0.10 -> 0.10");
-
-    // 0.10 -> cur
-    let enc = encrypt_rpgp_0_10(MSG, KEYFILE);
-    let dec = decrypt_rpgp_cur(&enc, KEYFILE);
-    assert_eq!(dec, MSG, "0.10 -> cur");
-
-    // cur -> 0.10
-    let enc = encrypt_rpgp_cur(MSG, KEYFILE);
-    let dec = decrypt_rpgp_0_10(&enc, KEYFILE);
-    assert_eq!(dec, MSG, "cur -> 0.10");
-
-    // cur -> cur
-    let enc = encrypt_rpgp_cur(MSG, KEYFILE);
-    let dec = decrypt_rpgp_cur(&enc, KEYFILE);
-    assert_eq!(dec, MSG, "cur -> cur");
-}
-
-fn decrypt_rpgp_0_10(enc_msg: &str, keyfile: &str) -> Vec<u8> {
-    use rpgp_0_10::Deserializable;
-
-    let (enc_msg, _) = rpgp_0_10::Message::from_string(enc_msg).unwrap();
-
-    let (ssk, _headers) =
-        rpgp_0_10::SignedSecretKey::from_armor_single(std::fs::File::open(keyfile).unwrap())
-            .expect("failed to read key");
-
-    let (mut dec, _) = enc_msg
-        .decrypt(|| "".to_string(), &[&ssk])
-        .expect("decrypt_rpgp_0_10");
-    let inner = dec.next().unwrap().unwrap();
-
-    inner.get_literal().unwrap().data().to_vec()
-}
-
-fn decrypt_rpgp_cur(enc_msg: &str, keyfile: &str) -> Vec<u8> {
-    use pgp::Deserializable;
-
-    let (enc_msg, _) = pgp::Message::from_string(enc_msg).expect("decrypt_rpgp_cur");
-
-    let (ssk, _headers) =
-        pgp::SignedSecretKey::from_armor_single(std::fs::File::open(keyfile).unwrap())
-            .expect("failed to read key");
-
-    let (dec, _) = enc_msg.decrypt(|| "".to_string(), &[&ssk]).unwrap();
-
-    dec.get_literal().unwrap().data().to_vec()
-}
-
-fn encrypt_rpgp_0_10(msg: &[u8], keyfile: &str) -> String {
-    use rpgp_0_10::crypto::sym::SymmetricKeyAlgorithm;
-    use rpgp_0_10::Deserializable;
-
-    let mut rng = ChaChaRng::from_seed([0u8; 32]);
-
-    let lit = rpgp_0_10::packet::LiteralData::from_bytes((&[]).into(), msg);
-    let msg = rpgp_0_10::Message::Literal(lit);
-
-    let (ssk, _headers) =
-        rpgp_0_10::SignedSecretKey::from_armor_single(std::fs::File::open(keyfile).unwrap())
-            .expect("failed to read key");
-
-    let enc = &ssk.secret_subkeys[0];
-
-    let enc_msg = msg
-        .encrypt_to_keys(&mut rng, SymmetricKeyAlgorithm::AES128, &[enc])
-        .unwrap();
-
-    enc_msg.to_armored_string(None).unwrap()
-}
-
-fn encrypt_rpgp_cur(msg: &[u8], keyfile: &str) -> String {
-    use pgp::crypto::sym::SymmetricKeyAlgorithm;
-    use pgp::{ArmorOptions, Deserializable};
-
-    let mut rng = ChaChaRng::from_seed([0u8; 32]);
-
-    let lit = pgp::packet::LiteralData::from_bytes((&[]).into(), msg);
-    let msg = pgp::Message::Literal(lit);
-
-    let (ssk, _headers) =
-        pgp::SignedSecretKey::from_armor_single(std::fs::File::open(keyfile).unwrap())
-            .expect("failed to read key");
-
-    let enc = &ssk.secret_subkeys[0];
-
-    let enc_msg = msg
-        .encrypt_to_keys_seipdv1(&mut rng, SymmetricKeyAlgorithm::AES128, &[enc])
-        .unwrap();
-
-    enc_msg.to_armored_string(ArmorOptions::default()).unwrap()
-}
