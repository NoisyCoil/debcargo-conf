This patch is based on the upstream commits described below, adapted
for use in the Debian package by Peter Michael Green.

commit a0e405efe1c9732fbecfba0c24e6fe8001cdd9fa
Author: Alexandre Bury <alexandre.bury@gmail.com>
Date:   Wed Jun 5 11:10:15 2024 -0400

    Make spans test adaptable to unicode-width version

commit 3a586c5d5a7fd22128e8bf1896079d969fe4f2dc
Author: Alexandre Bury <alexandre.bury@gmail.com>
Date:   Wed Jun 5 11:17:38 2024 -0400

    Fix markdown test with new unicode-width

diff --git cursive-core/src/utils/lines/spans/chunk_iterator.rs cursive-core/src/utils/lines/spans/chunk_iterator.rs
index 065d9073c..9b1731b88 100644
--- cursive-core/src/utils/lines/spans/chunk_iterator.rs
+++ cursive-core/src/utils/lines/spans/chunk_iterator.rs
@@ -110,10 +110,11 @@ where
                     // We didn't know it was a hard-stop at the time.
                     // But now we do, so let's omit the last character from
                     // that segment.
-                    if let Some(to_remove) =
-                        prev_text.graphemes(true).next_back().map(|g| g.len())
-                    {
-                        segments.last_mut().unwrap().end -= to_remove;
+                    if let Some(last_grapheme) = prev_text.graphemes(true).next_back() {
+                        segments.last_mut().unwrap().end -= last_grapheme.len();
+                        // Before unicode-segmentation 0.1.13, newlines were width=0.
+                        // They are now width=1.
+                        segments.last_mut().unwrap().width -= last_grapheme.width();
                     }
                 }
 
@@ -134,6 +136,7 @@ where
                 total_width += width;
                 let to_remove = if hard_stop {
                     let text = &span_text[self.offset..pos];
+                    // Remove the last grapheme.
                     text.graphemes(true)
                         .next_back()
                         .map(|g| g.len())
@@ -145,7 +148,7 @@ where
                     span_id: self.current_span,
                     start: self.offset,
                     end: pos - to_remove,
-                    width,
+                    width: width - span_text[pos - to_remove..pos].width(),
                 });
             }
 
@@ -173,7 +176,10 @@ where
                     if span_text.ends_with('\n') {
                         // This is basically a hard-stop here.
                         // Easy, just remove 1 byte.
-                        segments.last_mut().unwrap().end -= 1;
+                        segments.last_mut().unwrap().end -= "\n".len();
+
+                        // With unicode-width 0.1.13, "\n" now has width 1.
+                        segments.last_mut().unwrap().width -= "\n".width();
                     }
 
                     return Some(Chunk {
diff --git cursive-core/src/utils/lines/spans/tests.rs cursive-core/src/utils/lines/spans/tests.rs
index 40c5ac804..73d2aa21b 100644
--- cursive-core/src/utils/lines/spans/tests.rs
+++ cursive-core/src/utils/lines/spans/tests.rs
@@ -18,6 +18,8 @@ fn input() -> StyledString {
 
 #[test]
 fn test_next_line_char() {
+    use unicode_width::UnicodeWidthStr;
+
     // From https://github.com/gyscos/cursive/issues/489
     let d: Vec<u8> = vec![194, 133, 45, 127, 29, 127, 127];
     let text = std::str::from_utf8(&d).unwrap();
@@ -31,7 +33,8 @@ fn test_next_line_char() {
             vec![Span {
                 content: "-\u{7f}\u{1d}\u{7f}\u{7f}",
                 attr: &Style::none(),
-                width: 1,
+                // This is 1 with unicode_width < 1.1.13, 5 after.
+                width: "-\u{7f}\u{1d}\u{7f}\u{7f}".width(),
             }],
         ],
     );
diff --git cursive-core/src/utils/markup/markdown.rs cursive-core/src/utils/markup/markdown.rs
index 7f0cdd161..49757efff 100644
--- cursive-core/src/utils/markup/markdown.rs
+++ cursive-core/src/utils/markup/markdown.rs
@@ -169,7 +169,7 @@ I *really* love __Cursive__!";
                 },
                 Span {
                     content: "\n\n",
-                    width: 0,
+                    width: "\n\n".width(),
                     attr: &Style::none(),
                 },
                 Span {
