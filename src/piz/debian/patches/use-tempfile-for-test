From: Michael R. Crusoe <crusoe@debian.org>
Subject: Write test files to a temporary directory
Forwarded: https://github.com/mrkline/piz-rs/pull/14

When we run tests for Rust crates in Debian, the source code is read-only,
therefore we need this patch to write the files to a temporary directory.
--- piz.orig/tests/create-inputs.sh
+++ piz/tests/create-inputs.sh
@@ -1,10 +1,12 @@
 #!/bin/bash
 
-set -euo pipefail
+set -exuo pipefail
 
-cd tests/inputs
+echo "Setting up test environment in $1 …"
+mkdir -p $1/tests/inputs
+cp -r tests/inputs/hello $1/tests/inputs/
+cd $1/tests/inputs
 
-echo "Setting up test environment..."
 rm -f *.zip
 
 # Hello Zip archive (small text files)
--- piz.orig/tests/smoke.rs
+++ piz/tests/smoke.rs
@@ -1,3 +1,4 @@
+use std::env;
 use std::fs::File;
 use std::io;
 use std::process::Command;
@@ -22,13 +23,22 @@
     ];
 
     if inputs.iter().any(|i| !Utf8Path::new(i).exists()) {
-        Command::new("tests/create-inputs.sh")
+        let current_dir = env::current_dir()?;
+        let tempdir = tempfile::tempdir().unwrap();
+        let temp_path = tempdir.path();
+        Command::new("tests/create-inputs.sh").arg(temp_path.as_os_str())
             .status()
             .expect("Couldn't set up input files");
-    }
-
-    for input in &inputs {
-        read_zip(input)?;
+        assert!(env::set_current_dir(temp_path).is_ok());
+        for input in &inputs {
+            read_zip(input)?;
+        }
+        tempdir.close()?;
+        assert!(env::set_current_dir(current_dir).is_ok());
+    } else {
+        for input in &inputs {
+            read_zip(input)?;
+        }
     }
 
     Ok(())
--- piz.orig/Cargo.toml
+++ piz/Cargo.toml
@@ -81,6 +81,9 @@
 [dev-dependencies.structopt]
 version = "0.3"
 
+[dev-dependencies.tempfile]
+version = "3"
+
 [features]
 check-local-metadata = []
 default = ["check-local-metadata"]
