This patch is based on the upstream commit described below, adapted for use
in the Debian package by Peter Michael Green.

commit 1c5b27b054c44a0539577ac7017fdfdfb3729b42
Author: dependabot[bot] <49699333+dependabot[bot]@users.noreply.github.com>
Date:   Wed Feb 28 10:29:07 2024 +0000

    Update nix requirement from 0.27.1 to 0.28.0 (#41)
    
    * Update nix requirement from 0.27.1 to 0.28.0
    
    Updates the requirements on [nix](https://github.com/nix-rust/nix) to permit the latest version.
    - [Changelog](https://github.com/nix-rust/nix/blob/master/CHANGELOG.md)
    - [Commits](https://github.com/nix-rust/nix/compare/v0.27.1...v0.28.0)
    
    ---
    updated-dependencies:
    - dependency-name: nix
      dependency-type: direct:production
    ...
    
    Signed-off-by: dependabot[bot] <support@github.com>
    
    * fix: Compilation error from nix upgrade
    
    `nix::sys::socket::listen` now takes a backlog argument instead of an
    `i32`. In the case that `SOMAXCONN` is less than 128, we use that value.
    
    Signed-off-by: Jalil David Salamé Messina <jalil.salame@gmail.com>
    
    ---------
    
    Signed-off-by: dependabot[bot] <support@github.com>
    Signed-off-by: Jalil David Salamé Messina <jalil.salame@gmail.com>
    Co-authored-by: dependabot[bot] <49699333+dependabot[bot]@users.noreply.github.com>
    Co-authored-by: Jalil David Salamé Messina <jalil.salame@gmail.com>

Index: vsock/src/lib.rs
===================================================================
--- vsock.orig/src/lib.rs
+++ vsock/src/lib.rs
@@ -26,7 +26,7 @@ use nix::{
     sys::socket::{
         self, bind, connect, getpeername, getsockname, listen, recv, send, shutdown, socket,
         sockopt::{ReceiveTimeout, SendTimeout, SocketError},
-        AddressFamily, GetSockOpt, MsgFlags, SetSockOpt, SockFlag, SockType,
+        AddressFamily, Backlog, GetSockOpt, MsgFlags, SetSockOpt, SockFlag, SockType,
     },
 };
 use std::mem::size_of;
@@ -86,7 +86,7 @@ impl VsockListener {
         bind(socket.as_raw_fd(), addr)?;
 
         // rust stdlib uses a 128 connection backlog
-        listen(&socket, 128)?;
+        listen(&socket, Backlog::new(128).unwrap_or(Backlog::MAXCONN))?;
 
         Ok(Self { socket })
     }
Index: vsock/Cargo.toml
===================================================================
--- vsock.orig/Cargo.toml
+++ vsock/Cargo.toml
@@ -28,7 +28,7 @@ repository = "https://github.com/rust-vs
 version = "0.2.150"
 
 [dependencies.nix]
-version = "0.27.1"
+version = "0.29"
 features = [
     "ioctl",
     "socket",
