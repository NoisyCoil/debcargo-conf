From: Michael R. Crusoe <crusoe@debian.org>
Subject: Fix two tests that want to write to the current working directory
Forwarded: https://github.com/rust-rocksdb/rust-rocksdb/pull/938

To instead write to a temporary directory.

--- rocksdb.orig/src/compaction_filter.rs
+++ rocksdb/src/compaction_filter.rs
@@ -158,7 +158,11 @@
 fn compaction_filter_test() {
     use crate::{Options, DB};
 
-    let path = "_rust_rocksdb_filter_test";
+    let tempdir = tempfile::Builder::new()
+        .prefix("_rust_rocksdb_filter_test")
+        .tempdir()
+        .expect("Failed to create temporary path for the _rust_rocksdb_filter_test");
+    let path = tempdir.path();
     let mut opts = Options::default();
     opts.create_if_missing(true);
     opts.set_compaction_filter("test", test_filter);
--- rocksdb.orig/src/compaction_filter_factory.rs
+++ rocksdb/src/compaction_filter_factory.rs
@@ -122,7 +122,11 @@
 
     #[test]
     fn compaction_filter_factory_test() {
-        let path = "_rust_rocksdb_filter_factory_test";
+        let tempdir = tempfile::Builder::new()
+            .prefix("_rust_rocksdb_filter_factory_test")
+            .tempdir()
+            .expect("Failed to create temporary path for the _rust_rocksdb_filter_factory_test.");
+        let path = tempdir.path();
         let mut opts = Options::default();
         opts.create_if_missing(true);
         opts.set_compaction_filter_factory(TestFactory(CString::new("TestFactory").unwrap()));
--- rocksdb.orig/tests/test_comparator.rs
+++ rocksdb/tests/test_comparator.rs
@@ -13,7 +13,11 @@
 pub fn write_to_db_with_comparator(compare_fn: Box<CompareFn>) -> Vec<String> {
     let mut result_vec = Vec::new();
 
-    let path = "_path_for_rocksdb_storage";
+    let tempdir = tempfile::Builder::new()
+        .prefix("_path_for_rocksdb_storage")
+        .tempdir()
+        .expect("Failed to create temporary path for the _path_for_rocksdb_storage.");
+    let path = tempdir.path();
     {
         let mut db_opts = Options::default();
 
--- rocksdb.orig/tests/test_merge_operator.rs
+++ rocksdb/tests/test_merge_operator.rs
@@ -296,7 +296,11 @@
 #[test]
 fn test_merge_state() {
     use {Options, DB};
-    let path = "_rust_rocksdb_merge_test_state";
+    let tempdir = tempfile::Builder::new()
+        .prefix("_rust_rocksdb_merge_test_state")
+        .tempdir()
+        .expect("Failed to create temporary path for the _rust_rocksdb_merge_test_state.");
+    let path = tempdir.path();
     let mut opts = Options::default();
     opts.create_if_missing(true);
     opts.set_merge_operator_associative("max-limit-12", make_merge_max_with_limit(12));
