From: Maytham Alsudany <maytha8thedev@gmail.com>
Forwarded: https://github.com/danobi/prr/pull/64
Description: [PATCH] Add ability to generate manpages and shell completions
 This commit adds the ability to generate manpages and shell completions
 by enabling the "clap_mangen" and "clap_complete" features respectively.
 These are disabled for default since they are platform-specific.

--- /dev/null
+++ b/build.rs
@@ -0,0 +1,52 @@
+mod cli {
+    include!("src/cli.rs");
+}
+
+const LONG_ABOUT: &str =
+    "prr is a tool that brings mailing list style code reviews to Github PRs. This \
+means offline reviews and inline comments, more or less.
+
+To that end, prr introduces a new workflow for reviewing PRs:
+  1. Download the PR into a \"review file\" on your filesystem
+  2. Mark up the review file using your favorite text editor
+  3. Submit the review at your convenience
+
+For full documentation, please visit https://doc.dxuuu.xyz/prr/.";
+
+fn main() -> std::io::Result<()> {
+    if let Some(out_path) = std::env::var_os("GEN_DIR").or(std::env::var_os("OUT_DIR")) {
+        use clap::CommandFactory;
+        #[allow(unused_variables)]
+        let out_dir = std::path::PathBuf::from(out_path);
+        #[allow(unused_mut, unused_variables)]
+        let mut cmd = cli::Cli::command()
+            .author("Daniel Xu <dxu@apache.org>")
+            .about("Mailing list style code reviews for GitHub")
+            .long_about(LONG_ABOUT);
+
+        {
+            let man_dir = std::path::Path::join(&out_dir, "man");
+            std::fs::create_dir_all(&man_dir)?;
+            clap_mangen::generate_to(cmd.clone(), &man_dir)?;
+        }
+
+        {
+            use clap::ValueEnum;
+            let completions_dir = std::path::Path::join(&out_dir, "completions");
+            std::fs::create_dir_all(&completions_dir)?;
+            for shell in clap_complete::Shell::value_variants() {
+                clap_complete::generate_to(*shell, &mut cmd, "prr", &completions_dir)?;
+            }
+        }
+    }
+
+    println!(
+        "cargo:rustc-env=TARGET={}",
+        std::env::var("TARGET").unwrap()
+    );
+    println!("cargo:rerun-if-env-changed=GEN_DIR");
+    println!("cargo:rerun-if-env-changed=CARGO_FEATURE_CLAP_MANGEN");
+    println!("cargo:rerun-if-env-changed=CARGO_FEATURE_CLAP_COMPLETE");
+
+    Ok(())
+}
--- /dev/null
+++ b/src/cli.rs
@@ -0,0 +1,61 @@
+use clap::{Parser, Subcommand};
+use std::path::PathBuf;
+
+#[derive(Subcommand, Debug)]
+pub(crate) enum Command {
+    /// Get a pull request and begin a review
+    Get {
+        /// Ignore unsubmitted review checks
+        #[clap(short, long)]
+        force: bool,
+        /// Pull request to review (eg. `danobi/prr/24`)
+        pr: String,
+        /// Open review file in $EDITOR after download
+        #[clap(long)]
+        open: bool,
+    },
+    /// Open an existing review in $EDITOR
+    Edit {
+        /// Pull request to edit (eg. `danobi/prr/24`)
+        pr: String,
+    },
+    /// Submit a review
+    Submit {
+        /// Pull request to review (eg. `danobi/prr/24`)
+        pr: String,
+        #[clap(short, long)]
+        debug: bool,
+    },
+    /// Apply a pull request to the working directory
+    ///
+    /// This can be useful for building/testing PRs
+    Apply { pr: String },
+    /// Print a status summary of all known reviews
+    Status {
+        /// Hide column titles from output
+        #[clap(short, long)]
+        no_titles: bool,
+    },
+    /// Remove a review
+    Remove {
+        /// Pull requests to remove (eg. `danobi/prr/24`)
+        prs: Vec<String>,
+        /// Ignore unsubmitted review checks
+        #[clap(short, long)]
+        force: bool,
+        /// Remove submitted reviews in addition to provided reviews
+        #[clap(short, long)]
+        submitted: bool,
+    },
+}
+
+#[derive(Parser, Debug)]
+#[clap(version)]
+#[command(name = "prr")]
+pub struct Cli {
+    /// Path to config file
+    #[clap(long)]
+    pub(crate) config: Option<PathBuf>,
+    #[clap(subcommand)]
+    pub(crate) command: Command,
+}
--- a/src/main.rs
+++ b/src/main.rs
@@ -3,75 +3,19 @@
 use std::process;
 
 use anyhow::{bail, Context, Result};
-use clap::{Parser, Subcommand};
+use clap::Parser;
 
+mod cli;
 mod parser;
 mod prr;
 mod review;
 
+use cli::*;
 use prr::Prr;
 
 /// The name of the local configuration file
 pub const LOCAL_CONFIG_FILE_NAME: &str = ".prr.toml";
 
-#[derive(Subcommand, Debug)]
-enum Command {
-    /// Get a pull request and begin a review
-    Get {
-        /// Ignore unsubmitted review checks
-        #[clap(short, long)]
-        force: bool,
-        /// Pull request to review (eg. `danobi/prr/24`)
-        pr: String,
-        /// Open review file in $EDITOR after download
-        #[clap(long)]
-        open: bool,
-    },
-    /// Open an existing review in $EDITOR
-    Edit {
-        /// Pull request to edit (eg. `danobi/prr/24`)
-        pr: String,
-    },
-    /// Submit a review
-    Submit {
-        /// Pull request to review (eg. `danobi/prr/24`)
-        pr: String,
-        #[clap(short, long)]
-        debug: bool,
-    },
-    /// Apply a pull request to the working directory
-    ///
-    /// This can be useful for building/testing PRs
-    Apply { pr: String },
-    /// Print a status summary of all known reviews
-    Status {
-        /// Hide column titles from output
-        #[clap(short, long)]
-        no_titles: bool,
-    },
-    /// Remove a review
-    Remove {
-        /// Pull requests to remove (eg. `danobi/prr/24`)
-        prs: Vec<String>,
-        /// Ignore unsubmitted review checks
-        #[clap(short, long)]
-        force: bool,
-        /// Remove submitted reviews in addition to provided reviews
-        #[clap(short, long)]
-        submitted: bool,
-    },
-}
-
-#[derive(Parser, Debug)]
-#[clap(version)]
-struct Args {
-    /// Path to config file
-    #[clap(long)]
-    config: Option<PathBuf>,
-    #[clap(subcommand)]
-    command: Command,
-}
-
 /// Returns if exists the config file for the current project
 fn find_project_config_file() -> Option<PathBuf> {
     env::current_dir().ok().and_then(|mut path| loop {
@@ -109,7 +53,7 @@
 
 #[tokio::main]
 async fn main() -> Result<()> {
-    let args = Args::parse();
+    let args = Cli::parse();
 
     // Figure out where config file is
     let config_path = match args.config {
--- a/Cargo.toml
+++ b/Cargo.toml
@@ -71,3 +71,16 @@
 
 [dev-dependencies.tempfile]
 version = "3.8.1"
+
+[build-dependencies.anyhow]
+version = "1.0"
+
+[build-dependencies.clap]
+version = "4.4"
+features = ["derive"]
+
+[build-dependencies.clap_mangen]
+version = ">=0.2.20"
+
+[build-dependencies.clap_complete]
+version = "4"
