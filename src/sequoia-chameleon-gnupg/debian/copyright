Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: sequoia-chameleon-gnupg
Upstream-Contact: Justus Winter <justus@sequoia-pgp.org>
Source: https://gitlab.com/sequoia-pgp/sequoia-chameleon-gnupg

Files: *
Copyright:
 2021-2024 Justus Winter <justus@sequoia-pgp.org>
 2022 p≡p foundation
 2024 Sequoia PGP
License: GPL-3.0-or-later

Files:
 src/err-codes.h.in
Copyright:
 2003, 2004 g10 Code GmbH
License: LGPL-2.1-or-later

Files:
 AUTHORS.GnuPG
 src/dirmngr.option.c.fragment
 src/dirmngr.option.inc
 src/error_codes.inc
 src/gpg.option.gpg.c.fragment
 src/gpg.option.implicit
 src/gpg.option.inc
 src/gpg.option.oxidize
 src/Makefile
Copyright:
 1997-2019 Werner Koch
 2003-2021 g10 Code GmbH
 1994-2021 Free Software Foundation, Inc.
 2002 Klarälvdalens Datakonsult AB
 1995-1997, 2000-2007 Ulrich Drepper <drepper@gnu.ai.mit.edu>
 1994 X Consortium
 1998 by The Internet Society.
 1998-2004 The OpenLDAP Foundation
 1998-2004 Kurt D. Zeilenga.
 1998-2004 Net Boolean Incorporated.
 2001-2004 IBM Corporation.
 1999-2003 Howard Y.H. Chu.
 1999-2003 Symas Corporation.
 1998-2003 Hallvard B. Furuseth.
 1992-1996 Regents of the University of Michigan.
 2000 Dimitrios Souflis
 2008,2009,2010,2012-2016 William Ahern
 2015-2019 IBM Corporation
 2017 Bundesamt für Sicherheit in der Informationstechnik
 2021 James Bottomley <James.Bottomley@HansenPartnership.com>
 1998-2018 Free Software Foundation, Inc.
 1997-2018 Werner Koch
License: GPL-3.0-or-later

Files: debian/*
Copyright:
 2024 Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
 2024 Alexander Kjäll <alexander.kjall@gmail.com>
 2024 Holger Levsen <holger@debian.org>
License: GPL-3.0-or-later

License: LGPL-2.1-or-later
 Debian systems provide the LGPL 2.1 in /usr/share/common-licenses/LGPL-2.1

License: GPL-3.0-or-later
 Debian systems provide the GPL 3.0 in /usr/share/common-licenses/GPL-3
