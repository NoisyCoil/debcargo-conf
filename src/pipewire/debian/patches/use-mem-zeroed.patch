Description: use mem::zeroed to create zero timespec.
 When time64 is enabled, glibc adds padding to struct timespec,
 on time64 architectures.Unfortunately this means that timespecs can't
 be created in the normal way in rust.

 Instead we create the structure using std::mem::zeroed and use
 member access to fill it in.
Author: Peter Michael Green <plugwash@debian.org>

--- rust-pipewire-0.8.0.orig/src/loop_.rs
+++ rust-pipewire-0.8.0/src/loop_.rs
@@ -633,10 +633,10 @@ impl<'l> TimerSource<'l> {
     /// The provided durations seconds must fit in an i64. Otherwise, this function will panic.
     pub fn update_timer(&self, value: Option<Duration>, interval: Option<Duration>) -> SpaResult {
         fn duration_to_timespec(duration: Duration) -> spa_sys::timespec {
-            spa_sys::timespec {
-                tv_sec: duration.as_secs().try_into().expect("Duration too long"),
-                tv_nsec: duration.subsec_nanos().try_into().unwrap(),
-            }
+            let mut result: spa_sys::timespec = unsafe { std::mem::zeroed() };
+            result.tv_sec = duration.as_secs().try_into().expect("Duration too long");
+            result.tv_nsec = duration.subsec_nanos().try_into().unwrap();
+            result
         }
 
         let value = duration_to_timespec(value.unwrap_or_default());
--- rust-pipewire-0.8.0.orig/src/thread_loop.rs
+++ rust-pipewire-0.8.0/src/thread_loop.rs
@@ -148,13 +148,12 @@ impl ThreadLoop {
     /// to get a suitable timespec
     pub fn timed_wait_full(&self, abstime: nix::sys::time::TimeSpec) {
         unsafe {
-            let mut abstime = pw_sys::timespec {
-                tv_sec: abstime.tv_sec(),
-                tv_nsec: abstime.tv_nsec(),
-            };
+            let mut abstimepw: pw_sys::timespec = std::mem::zeroed();
+            abstimepw.tv_sec = abstime.tv_sec();
+            abstimepw.tv_nsec = abstime.tv_nsec();
             pw_sys::pw_thread_loop_timed_wait_full(
                 self.as_raw_ptr(),
-                &mut abstime as *mut pw_sys::timespec,
+                &mut abstimepw as *mut pw_sys::timespec,
             );
         }
     }
