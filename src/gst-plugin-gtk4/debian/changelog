rust-gst-plugin-gtk4 (0.13.1-1) unstable; urgency=medium

  * Package gst-plugin-gtk4 0.13.1 from crates.io using debcargo 2.7.0
  * d/control: Update to S-V 4.7.0; switch to dh-compat; updated
    Provides and Depends
  * remove-windows-deps.diff: Refresh for new upstream
  * d/copyright: Update for new upstream

 -- Matthias Geiger <werdahias@debian.org>  Mon, 23 Sep 2024 01:12:57 +0200

rust-gst-plugin-gtk4 (0.13.0-4) unstable; urgency=medium

  * Team upload
  * Package gst-plugin-gtk4 0.13.0 from crates.io using debcargo 2.6.1
  * Stop ignoring gtk 4.14 & dmabuf test failures

 -- Jeremy Bícha <jbicha@ubuntu.com>  Mon, 02 Sep 2024 16:54:14 -0400

rust-gst-plugin-gtk4 (0.13.0-3) unstable; urgency=medium

  * Team upload.
  * Package gst-plugin-gtk4 0.13.0 from crates.io using debcargo 2.6.1
  * Release to unstable

 -- Jeremy Bícha <jbicha@ubuntu.com>  Tue, 27 Aug 2024 14:48:57 -0400

rust-gst-plugin-gtk4 (0.13.0-2) experimental; urgency=medium

  * d/control: Updated for new upstream release 

 -- Matthias Geiger <werdahias@debian.org>  Thu, 22 Aug 2024 15:04:39 +0200

rust-gst-plugin-gtk4 (0.13.0-1) experimental; urgency=medium

  * Team upload.
  * Package gst-plugin-gtk4 0.13.0 from crates.io using debcargo 2.6.1
  * Mark v1_26 feature as broken

 -- Jeremy Bícha <jbicha@ubuntu.com>  Wed, 21 Aug 2024 19:37:13 -0400

rust-gst-plugin-gtk4 (0.12.7-5) unstable; urgency=medium

  * Upload to unstable 

 -- Matthias Geiger <werdahias@debian.org>  Sun, 04 Aug 2024 14:27:35 +0200

rust-gst-plugin-gtk4 (0.12.7-4) experimental; urgency=medium

  * Build gstreamer1.0-gtk4 package (Closes: #1070646) 

 -- Matthias Geiger <werdahias@debian.org>  Fri, 02 Aug 2024 18:17:14 +0200

rust-gst-plugin-gtk4 (0.12.7-3) unstable; urgency=medium

  * Drop gst-allocators patch since this dependency is now in Debian
  * Add patch to disable static feature
  * Mark dmabuf feature as flaky, too

 -- Matthias Geiger <werdahias@riseup.net>  Sun, 21 Jul 2024 16:55:49 +0200

rust-gst-plugin-gtk4 (0.12.7-2) unstable; urgency=medium

  * Upload to unstable 

 -- Matthias Geiger <werdahias@riseup.net>  Sun, 14 Jul 2024 15:17:07 +0200

rust-gst-plugin-gtk4 (0.12.7-1) experimental; urgency=medium

  * Package gst-plugin-gtk4 0.12.7 from crates.io using debcargo 2.6.1
  * Stop patching async-channel dependency

 -- Matthias Geiger <werdahias@riseup.net>  Mon, 24 Jun 2024 20:24:30 +0200

rust-gst-plugin-gtk4 (0.12.5-2) unstable; urgency=medium

  * Add patch to disable gst-allocator dependency (and subsequent dmabuf
    feature)

 -- Matthias Geiger <werdahias@riseup.net>  Mon, 06 May 2024 12:23:59 +0200

rust-gst-plugin-gtk4 (0.12.5-1) unstable; urgency=medium

  * Package gst-plugin-gtk4 0.12.5 from crates.io using debcargo 2.6.1
  * Updated copyright years
  * New upstream release (Closes: #1070191)

 -- Matthias Geiger <werdahias@riseup.net>  Sun, 05 May 2024 15:32:05 +0200

rust-gst-plugin-gtk4 (0.12.3-1) experimental; urgency=medium

  * Team upload
  * Package gst-plugin-gtk4 0.12.3 from crates.io using debcargo 2.6.1

 -- Jeremy Bícha <jbicha@ubuntu.com>  Wed, 17 Apr 2024 17:30:57 -0400

rust-gst-plugin-gtk4 (0.12.1-2) experimental; urgency=medium

  * Mark gtk_v4_14 and gst-gl features as broken 

 -- Matthias Geiger <werdahias@riseup.net>  Sat, 09 Mar 2024 20:06:15 +0100

rust-gst-plugin-gtk4 (0.12.1-1) experimental; urgency=medium

  * Package gst-plugin-gtk4 0.12.1 from crates.io using debcargo 2.6.1
  * Rebased patches for new upstream

 -- Matthias Geiger <werdahias@riseup.net>  Tue, 27 Feb 2024 18:58:20 +0100

rust-gst-plugin-gtk4 (0.11.3-2) unstable; urgency=medium

  * Mark gst_gl feature as flaky

 -- Matthias Geiger <werdahias@riseup.net>  Sun, 14 Jan 2024 15:25:27 +0100

rust-gst-plugin-gtk4 (0.11.3-1) unstable; urgency=medium

  * Package gst-plugin-gtk4 0.11.3 from crates.io using debcargo 2.6.1
  * Rebased patch for new upstream; relaxed async-channel dependency
  * Upload to unstable

 -- Matthias Geiger <werdahias@riseup.net>  Sat, 13 Jan 2024 18:50:20 +0100

rust-gst-plugin-gtk4 (0.11.2-1) experimental; urgency=medium

  * Package gst-plugin-gtk4 0.11.2 from crates.io using debcargo 2.6.1

 -- Matthias Geiger <werdahias@riseup.net>  Tue, 09 Jan 2024 16:53:15 -0500
