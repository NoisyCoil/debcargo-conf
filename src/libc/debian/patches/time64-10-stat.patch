This patch is based on the commit described below, taken from upstream pull
request 3175, it was adapted for use in the Debian package by Peter Michael
Green.

commit 3e37f1b0ca3ca56f8a18a0d46075a12f89838dc6
Author: Ola x Nilsson <olani@axis.com>
Date:   Mon Mar 4 11:13:49 2024 +0100

    gnu: Adapt stat, statfs, statvfs for _TIME_BITS=64
    
    Adapt the 32-bit versions of the various stat and stat*64 structs to
    support 64 bit time and file offset.
    
    Move the default stat struct from b32/mod.rs to a new file
    fnu/b32/generic.rs as it now only supports _most_ platforms.
    Some archs still need their own version.
    
    stat64, statfs64 and statvfs64 becomes aliases to stat, statfs and
    statfcf respectively for arm, riscv32 and sparc.
    
    statfs64 becomes an alias for statfs on mips and powerpc.

diff --git a/src/unix/linux_like/linux/gnu/b32/arm/mod.rs b/src/unix/linux_like/linux/gnu/b32/arm/mod.rs
index 454767a9f5..d77f1389e1 100644
--- a/src/unix/linux_like/linux/gnu/b32/arm/mod.rs
+++ b/src/unix/linux_like/linux/gnu/b32/arm/mod.rs
@@ -55,59 +55,6 @@ s! {
         __unused2: ::c_ulong
     }
 
-    pub struct stat64 {
-        pub st_dev: ::dev_t,
-        __pad1: ::c_uint,
-        __st_ino: ::ino_t,
-        pub st_mode: ::mode_t,
-        pub st_nlink: ::nlink_t,
-        pub st_uid: ::uid_t,
-        pub st_gid: ::gid_t,
-        pub st_rdev: ::dev_t,
-        __pad2: ::c_uint,
-        pub st_size: ::off64_t,
-        pub st_blksize: ::blksize_t,
-        pub st_blocks: ::blkcnt64_t,
-        pub st_atime: ::time_t,
-        pub st_atime_nsec: ::c_long,
-        pub st_mtime: ::time_t,
-        pub st_mtime_nsec: ::c_long,
-        pub st_ctime: ::time_t,
-        pub st_ctime_nsec: ::c_long,
-        pub st_ino: ::ino64_t,
-    }
-
-    pub struct statfs64 {
-        pub f_type: ::__fsword_t,
-        pub f_bsize: ::__fsword_t,
-        pub f_blocks: u64,
-        pub f_bfree: u64,
-        pub f_bavail: u64,
-        pub f_files: u64,
-        pub f_ffree: u64,
-        pub f_fsid: ::fsid_t,
-        pub f_namelen: ::__fsword_t,
-        pub f_frsize: ::__fsword_t,
-        pub f_flags: ::__fsword_t,
-        pub f_spare: [::__fsword_t; 4],
-    }
-
-    pub struct statvfs64 {
-        pub f_bsize: ::c_ulong,
-        pub f_frsize: ::c_ulong,
-        pub f_blocks: u64,
-        pub f_bfree: u64,
-        pub f_bavail: u64,
-        pub f_files: u64,
-        pub f_ffree: u64,
-        pub f_favail: u64,
-        pub f_fsid: ::c_ulong,
-        __f_unused: ::c_int,
-        pub f_flag: ::c_ulong,
-        pub f_namemax: ::c_ulong,
-        __f_spare: [::c_int; 6],
-    }
-
     pub struct shmid_ds {
         pub shm_perm: ::ipc_perm,
         pub shm_segsz: ::size_t,
diff --git a/src/unix/linux_like/linux/gnu/b32/generic.rs b/src/unix/linux_like/linux/gnu/b32/generic.rs
new file mode 100644
index 0000000000..fd1109aa91
--- /dev/null
+++ b/src/unix/linux_like/linux/gnu/b32/generic.rs
@@ -0,0 +1,30 @@
+s! {
+    pub struct stat {
+        pub st_dev: ::dev_t,
+        #[cfg(not(gnu_time64_abi))]
+        _pad1: i32,
+        pub st_ino: ::ino_t,
+        pub st_mode: ::mode_t,
+        pub st_nlink: ::nlink_t,
+        pub st_uid: ::uid_t,
+        pub st_gid: ::gid_t,
+        pub st_rdev: ::dev_t,
+        #[cfg(not(gnu_time64_abi))]
+        __pad2: i32,
+        pub st_size: ::off_t,
+        pub st_blksize: ::blksize_t,
+        pub st_blocks: ::blkcnt_t,
+        pub st_atime: ::time_t,
+        pub st_atime_nsec: ::c_long,
+        #[cfg(gnu_time64_abi)]
+        __pad3: i32,
+        pub st_mtime: ::time_t,
+        pub st_mtime_nsec: ::c_long,
+        #[cfg(gnu_time64_abi)]
+        __pad4: i32,
+        pub st_ctime: ::time_t,
+        pub st_ctime_nsec: ::c_long,
+        #[cfg(gnu_time64_abi)]
+        __pad5: i32,
+    }
+}
diff --git a/src/unix/linux_like/linux/gnu/b32/mips/mod.rs b/src/unix/linux_like/linux/gnu/b32/mips/mod.rs
index 8f5ed0f348..df50a49ee5 100644
--- a/src/unix/linux_like/linux/gnu/b32/mips/mod.rs
+++ b/src/unix/linux_like/linux/gnu/b32/mips/mod.rs
@@ -1,30 +1,90 @@
 pub type c_char = i8;
 pub type wchar_t = i32;
 
+pub type statfs64 = statfs;
 s! {
-    pub struct stat64 {
+    pub struct stat {
+        #[cfg(gnu_time64_abi)]
+        pub st_dev: ::dev_t,
+        #[cfg(not(gnu_time64_abi))]
         pub st_dev: ::c_ulong,
+        #[cfg(not(gnu_time64_abi))]
         st_pad1: [::c_long; 3],
-        pub st_ino: ::ino64_t,
+
+        pub st_ino: ::ino_t,
         pub st_mode: ::mode_t,
         pub st_nlink: ::nlink_t,
         pub st_uid: ::uid_t,
         pub st_gid: ::gid_t,
+        #[cfg(gnu_time64_abi)]
+        pub st_rdev: ::dev_t,
+        #[cfg(not(gnu_time64_abi))]
         pub st_rdev: ::c_ulong,
+
+        #[cfg(not(gnu_time64_abi))]
         st_pad2: [::c_long; 2],
-        pub st_size: ::off64_t,
+
+
+        pub st_size: ::off_t,
+
+        #[cfg(not(gnu_time64_abi))]
+        st_pad3: ::c_long,
+
+        #[cfg(gnu_time64_abi)]
+        pub st_blksize: ::blksize_t,
+        #[cfg(gnu_time64_abi)]
+        pub st_blocks: ::blkcnt_t,
+
         pub st_atime: ::time_t,
+        #[cfg(gnu_time64_abi)]
+        __pad1: i32,
         pub st_atime_nsec: ::c_long,
+        #[cfg(not(gnu_time64_abi))]
+        __pad1: i32,
         pub st_mtime: ::time_t,
+        #[cfg(gnu_time64_abi)]
+        __pad2: i32,
         pub st_mtime_nsec: ::c_long,
+        #[cfg(not(gnu_time64_abi))]
+        __pad2: i32,
         pub st_ctime: ::time_t,
+        #[cfg(gnu_time64_abi)]
+        __pad3: i32,
         pub st_ctime_nsec: ::c_long,
+        #[cfg(not(gnu_time64_abi))]
+        __pad3: i32,
+
+
+        #[cfg(not(gnu_time64_abi))]
         pub st_blksize: ::blksize_t,
-        st_pad3: ::c_long,
-        pub st_blocks: ::blkcnt64_t,
+        #[cfg(not(gnu_time64_abi))]
+        pub st_blocks: ::blkcnt_t,
+        #[cfg(not(gnu_time64_abi))]
         st_pad5: [::c_long; 14],
     }
 
+    pub struct stat64 {
+        pub st_dev: ::dev_t,
+        pub st_ino: ::ino64_t,
+        pub st_mode: ::mode_t,
+        pub st_nlink: ::nlink_t,
+        pub st_uid: ::uid_t,
+        pub st_gid: ::gid_t,
+        pub st_rdev: ::dev_t,
+        pub st_size: ::off64_t,
+        pub st_blksize: ::blksize_t,
+        pub st_blocks: ::blkcnt64_t,
+        pub st_atime: ::time_t,
+        __pad1: i32,
+        pub st_atime_nsec: ::c_long,
+        pub st_mtime: ::time_t,
+        __pad2: i32,
+        pub st_mtime_nsec: ::c_long,
+        pub st_ctime: ::time_t,
+        __pad3: i32,
+        pub st_ctime_nsec: ::c_long,
+    }
+
     pub struct statfs {
         pub f_type: ::c_long,
         pub f_bsize: ::c_long,
@@ -42,17 +102,2 @@ s! {
 
-    pub struct statfs64 {
-        pub f_type: ::c_long,
-        pub f_bsize: ::c_long,
-        pub f_frsize: ::c_long,
-        pub f_blocks: u64,
-        pub f_bfree: u64,
-        pub f_files: u64,
-        pub f_ffree: u64,
-        pub f_bavail: u64,
-        pub f_fsid: ::fsid_t,
-        pub f_namelen: ::c_long,
-        pub f_flags: ::c_long,
-        pub f_spare: [::c_long; 5],
-    }
-
     pub struct statvfs64 {
diff --git a/src/unix/linux_like/linux/gnu/b32/mod.rs b/src/unix/linux_like/linux/gnu/b32/mod.rs
index 471d7ad41c..0b14d5b761 100644
--- a/src/unix/linux_like/linux/gnu/b32/mod.rs
+++ b/src/unix/linux_like/linux/gnu/b32/mod.rs
@@ -52,56 +52,15 @@ cfg_if! {
         pub type blksize_t = i32;
     }
 }
-
-s! {
-    pub struct stat {
-        #[cfg(not(any(target_arch = "mips", target_arch = "mips32r6")))]
-        pub st_dev: ::dev_t,
-        #[cfg(any(target_arch = "mips", target_arch = "mips32r6"))]
-        pub st_dev: ::c_ulong,
-
-        #[cfg(not(any(target_arch = "mips", target_arch = "mips32r6")))]
-        __pad1: ::c_short,
-        #[cfg(any(target_arch = "mips", target_arch = "mips32r6"))]
-        st_pad1: [::c_long; 3],
-        pub st_ino: ::ino_t,
-        pub st_mode: ::mode_t,
-        pub st_nlink: ::nlink_t,
-        pub st_uid: ::uid_t,
-        pub st_gid: ::gid_t,
-        #[cfg(not(any(target_arch = "mips", target_arch = "mips32r6")))]
-        pub st_rdev: ::dev_t,
-        #[cfg(any(target_arch = "mips", target_arch = "mips32r6"))]
-        pub st_rdev: ::c_ulong,
-        #[cfg(not(any(target_arch = "mips", target_arch = "mips32r6")))]
-        __pad2: ::c_short,
-        #[cfg(any(target_arch = "mips", target_arch = "mips32r6"))]
-        st_pad2: [::c_long; 2],
-        pub st_size: ::off_t,
-        #[cfg(any(target_arch = "mips", target_arch = "mips32r6"))]
-        st_pad3: ::c_long,
-        #[cfg(not(any(target_arch = "mips", target_arch = "mips32r6")))]
-        pub st_blksize: ::blksize_t,
-        #[cfg(not(any(target_arch = "mips", target_arch = "mips32r6")))]
-        pub st_blocks: ::blkcnt_t,
-        pub st_atime: ::time_t,
-        pub st_atime_nsec: ::c_long,
-        pub st_mtime: ::time_t,
-        pub st_mtime_nsec: ::c_long,
-        pub st_ctime: ::time_t,
-        pub st_ctime_nsec: ::c_long,
-        #[cfg(not(any(target_arch = "mips", target_arch = "mips32r6")))]
-        __unused4: ::c_long,
-        #[cfg(not(any(target_arch = "mips", target_arch = "mips32r6")))]
-        __unused5: ::c_long,
-        #[cfg(any(target_arch = "mips", target_arch = "mips32r6"))]
-        pub st_blksize: ::blksize_t,
-        #[cfg(any(target_arch = "mips", target_arch = "mips32r6"))]
-        pub st_blocks: ::blkcnt_t,
-        #[cfg(any(target_arch = "mips", target_arch = "mips32r6"))]
-        st_pad5: [::c_long; 14],
+cfg_if! {
+    if #[cfg(any(target_arch = "arm", target_arch="riscv32", target_arch="sparc"))] {
+        pub type stat64 = stat;
+        pub type statfs64 = statfs;
+        pub type statvfs64 = statvfs;
     }
+}
 
+s! {
     pub struct statvfs {
         pub f_bsize: ::c_ulong,
         pub f_frsize: ::c_ulong,
@@ -445,3 +404,16 @@ cfg_if! {
         // Unknown target_arch
     }
 }
+
+cfg_if! {
+    if #[cfg(not(any(
+        target_arch = "mips",
+        target_arch = "mips32r6",
+        target_arch = "powerpc",
+        target_arch = "riscv32",
+        target_arch = "sparc",
+    )))] {
+        mod generic;
+        pub use self::generic::*;
+    }
+}
diff --git a/src/unix/linux_like/linux/gnu/b32/powerpc.rs b/src/unix/linux_like/linux/gnu/b32/powerpc.rs
index dd5732e0dc..af4f13954b 100644
--- a/src/unix/linux_like/linux/gnu/b32/powerpc.rs
+++ b/src/unix/linux_like/linux/gnu/b32/powerpc.rs
@@ -1,6 +1,8 @@
 pub type c_char = u8;
 pub type wchar_t = i32;
 
+pub type statfs64 = statfs;
+
 s! {
     pub struct sigaction {
         pub sa_sigaction: ::sighandler_t,
@@ -9,6 +11,38 @@ s! {
         pub sa_restorer: ::Option<extern fn()>,
     }
 
+    pub struct stat {
+        pub st_dev: ::dev_t,
+        pub st_ino: ::ino_t,
+        pub st_mode: ::mode_t,
+        pub st_nlink: ::nlink_t,
+        pub st_uid: ::uid_t,
+        pub st_gid: ::gid_t,
+        pub st_rdev: ::dev_t,
+        pub st_size: ::off_t,
+        pub st_blksize: ::blksize_t,
+        pub st_blocks: ::blkcnt_t,
+        pub st_atime: ::time_t,
+        #[cfg(gnu_time64_abi)]
+        __pad1: i32,
+        pub st_atime_nsec: ::c_long,
+        #[cfg(not(gnu_time64_abi))]
+        __pad1: i32,
+        pub st_mtime: ::time_t,
+        #[cfg(gnu_time64_abi)]
+        __pad2: i32,
+        pub st_mtime_nsec: ::c_long,
+        #[cfg(not(gnu_time64_abi))]
+        __pad2: i32,
+        pub st_ctime: ::time_t,
+        #[cfg(gnu_time64_abi)]
+        __pad3: i32,
+        pub st_ctime_nsec: ::c_long,
+        #[cfg(not(gnu_time64_abi))]
+        __pad3: i32,
+    }
+
+
     pub struct statfs {
         pub f_type: ::__fsword_t,
         pub f_bsize: ::__fsword_t,
@@ -62,33 +96,18 @@ s! {
         pub st_uid: ::uid_t,
         pub st_gid: ::gid_t,
         pub st_rdev: ::dev_t,
-        __pad2: ::c_ushort,
         pub st_size: ::off64_t,
         pub st_blksize: ::blksize_t,
         pub st_blocks: ::blkcnt64_t,
         pub st_atime: ::time_t,
-        pub st_atime_nsec: ::c_long,
+         __pad1: i32,
+       pub st_atime_nsec: ::c_long,
         pub st_mtime: ::time_t,
+        __pad2: i32,
         pub st_mtime_nsec: ::c_long,
         pub st_ctime: ::time_t,
+        __pad3: i32,
         pub st_ctime_nsec: ::c_long,
-        __glibc_reserved4: ::c_ulong,
-        __glibc_reserved5: ::c_ulong,
-    }
-
-    pub struct statfs64 {
-        pub f_type: ::__fsword_t,
-        pub f_bsize: ::__fsword_t,
-        pub f_blocks: u64,
-        pub f_bfree: u64,
-        pub f_bavail: u64,
-        pub f_files: u64,
-        pub f_ffree: u64,
-        pub f_fsid: ::fsid_t,
-        pub f_namelen: ::__fsword_t,
-        pub f_frsize: ::__fsword_t,
-        pub f_flags: ::__fsword_t,
-        pub f_spare: [::__fsword_t; 4],
     }
 
     pub struct statvfs64 {
diff --git a/src/unix/linux_like/linux/gnu/b32/riscv32/mod.rs b/src/unix/linux_like/linux/gnu/b32/riscv32/mod.rs
index 8a75e6d42b..f094ab7603 100644
--- a/src/unix/linux_like/linux/gnu/b32/riscv32/mod.rs
+++ b/src/unix/linux_like/linux/gnu/b32/riscv32/mod.rs
@@ -44,28 +44,6 @@ s! {
         __unused: [::c_int; 2usize],
     }
 
-    pub struct stat64 {
-        pub st_dev: ::dev_t,
-        pub st_ino: ::ino64_t,
-        pub st_mode: ::mode_t,
-        pub st_nlink: ::nlink_t,
-        pub st_uid: ::uid_t,
-        pub st_gid: ::gid_t,
-        pub st_rdev: ::dev_t,
-        pub __pad1: ::dev_t,
-        pub st_size: ::off64_t,
-        pub st_blksize: ::blksize_t,
-        pub __pad2: ::c_int,
-        pub st_blocks: ::blkcnt64_t,
-        pub st_atime: ::time_t,
-        pub st_atime_nsec: ::c_long,
-        pub st_mtime: ::time_t,
-        pub st_mtime_nsec: ::c_long,
-        pub st_ctime: ::time_t,
-        pub st_ctime_nsec: ::c_long,
-        __unused: [::c_int; 2],
-    }
-
     pub struct statfs {
         pub f_type: ::c_long,
         pub f_bsize: ::c_long,
@@ -81,21 +59,6 @@ s! {
         pub f_spare: [::c_long; 4],
     }
 
-    pub struct statfs64 {
-        pub f_type: ::c_long,
-        pub f_bsize: ::c_long,
-        pub f_blocks: ::fsblkcnt64_t,
-        pub f_bfree: ::fsblkcnt64_t,
-        pub f_bavail: ::fsblkcnt64_t,
-        pub f_files: ::fsfilcnt64_t,
-        pub f_ffree: ::fsfilcnt64_t,
-        pub f_fsid: ::fsid_t,
-        pub f_namelen: ::c_long,
-        pub f_frsize: ::c_long,
-        pub f_flags: ::c_long,
-        pub f_spare: [::c_long; 4],
-    }
-
     pub struct statvfs {
         pub f_bsize: ::c_ulong,
         pub f_frsize: ::c_ulong,
@@ -111,21 +74,6 @@ s! {
         pub __f_spare: [::c_int; 6],
     }
 
-    pub struct statvfs64 {
-        pub f_bsize: ::c_ulong,
-        pub f_frsize: ::c_ulong,
-        pub f_blocks: ::fsblkcnt64_t,
-        pub f_bfree: ::fsblkcnt64_t,
-        pub f_bavail: ::fsblkcnt64_t,
-        pub f_files: ::fsfilcnt64_t,
-        pub f_ffree: ::fsfilcnt64_t,
-        pub f_favail: ::fsfilcnt64_t,
-        pub f_fsid: ::c_ulong,
-        pub f_flag: ::c_ulong,
-        pub f_namemax: ::c_ulong,
-        pub __f_spare: [::c_int; 6],
-    }
-
     pub struct siginfo_t {
         pub si_signo: ::c_int,
         pub si_errno: ::c_int,
diff --git a/src/unix/linux_like/linux/gnu/b32/sparc/mod.rs b/src/unix/linux_like/linux/gnu/b32/sparc/mod.rs
index 16b836f7e6..87a650475c 100644
--- a/src/unix/linux_like/linux/gnu/b32/sparc/mod.rs
+++ b/src/unix/linux_like/linux/gnu/b32/sparc/mod.rs
@@ -71,50 +71,21 @@ s! {
         pub st_blksize: ::blksize_t,
         pub st_blocks: ::blkcnt64_t,
         pub st_atime: ::time_t,
+        #[cfg(gnu_time64_abi)]
+        __pad3: i32,
         pub st_atime_nsec: ::c_long,
         pub st_mtime: ::time_t,
+        #[cfg(gnu_time64_abi)]
+        __pad4: i32,
         pub st_mtime_nsec: ::c_long,
         pub st_ctime: ::time_t,
+        #[cfg(gnu_time64_abi)]
+        __pad5: i32,
         pub st_ctime_nsec: ::c_long,
+        #[cfg(not(gnu_time64_abi))]
         __unused: [::c_long; 2],
     }
 
-    pub struct stat64 {
-        pub st_dev: ::dev_t,
-        pub st_ino: ::ino64_t,
-        pub st_mode: ::mode_t,
-        pub st_nlink: ::nlink_t,
-        pub st_uid: ::uid_t,
-        pub st_gid: ::gid_t,
-        pub st_rdev: ::dev_t,
-        __pad2: ::c_ushort,
-        pub st_size: ::off64_t,
-        pub st_blksize: ::blksize_t,
-        pub st_blocks: ::blkcnt64_t,
-        pub st_atime: ::time_t,
-        pub st_atime_nsec: ::c_long,
-        pub st_mtime: ::time_t,
-        pub st_mtime_nsec: ::c_long,
-        pub st_ctime: ::time_t,
-        pub st_ctime_nsec: ::c_long,
-        __reserved: [::c_long; 2],
-    }
-
-    pub struct statfs64 {
-        pub f_type: ::__fsword_t,
-        pub f_bsize: ::__fsword_t,
-        pub f_blocks: u64,
-        pub f_bfree: u64,
-        pub f_bavail: u64,
-        pub f_files: u64,
-        pub f_ffree: u64,
-        pub f_fsid: ::fsid_t,
-        pub f_namelen: ::__fsword_t,
-        pub f_frsize: ::__fsword_t,
-        pub f_flags: ::__fsword_t,
-        pub f_spare: [::__fsword_t; 4],
-    }
-
     pub struct statvfs {
         pub f_bsize: ::c_ulong,
         pub f_frsize: ::c_ulong,
@@ -130,21 +101,6 @@ s! {
         __f_spare: [::c_int; 6],
     }
 
-    pub struct statvfs64 {
-        pub f_bsize: ::c_ulong,
-        pub f_frsize: ::c_ulong,
-        pub f_blocks: u64,
-        pub f_bfree: u64,
-        pub f_bavail: u64,
-        pub f_files: u64,
-        pub f_ffree: u64,
-        pub f_favail: u64,
-        pub f_fsid: ::c_ulong,
-        pub f_flag: ::c_ulong,
-        pub f_namemax: ::c_ulong,
-        __f_spare: [::c_int; 6],
-    }
-
     pub struct ipc_perm {
         pub __key: ::key_t,
         pub uid: ::uid_t,
diff --git a/src/unix/linux_like/linux/gnu/b32/x86/mod.rs b/src/unix/linux_like/linux/gnu/b32/x86/mod.rs
index 2acac7fb7a..5471a8c992 100644
--- a/src/unix/linux_like/linux/gnu/b32/x86/mod.rs
+++ b/src/unix/linux_like/linux/gnu/b32/x86/mod.rs
@@ -2,6 +2,8 @@ pub type c_char = i8;
 pub type wchar_t = i32;
 pub type greg_t = i32;
 
+pub type statfs64 = statfs;
+
 s! {
     pub struct sigaction {
         pub sa_sigaction: ::sighandler_t,
@@ -131,39 +133,24 @@ s! {
 
     pub struct stat64 {
         pub st_dev: ::dev_t,
-        __pad1: ::c_uint,
-        __st_ino: ::ino_t,
+        pub st_ino: ::ino_t,
         pub st_mode: ::mode_t,
         pub st_nlink: ::nlink_t,
         pub st_uid: ::uid_t,
         pub st_gid: ::gid_t,
         pub st_rdev: ::dev_t,
-        __pad2: ::c_uint,
         pub st_size: ::off64_t,
         pub st_blksize: ::blksize_t,
         pub st_blocks: ::blkcnt64_t,
         pub st_atime: ::time_t,
         pub st_atime_nsec: ::c_long,
+        __pad1: i32,
         pub st_mtime: ::time_t,
         pub st_mtime_nsec: ::c_long,
+        __pad2: i32,
         pub st_ctime: ::time_t,
         pub st_ctime_nsec: ::c_long,
-        pub st_ino: ::ino64_t,
-    }
-
-    pub struct statfs64 {
-        pub f_type: ::__fsword_t,
-        pub f_bsize: ::__fsword_t,
-        pub f_blocks: u64,
-        pub f_bfree: u64,
-        pub f_bavail: u64,
-        pub f_files: u64,
-        pub f_ffree: u64,
-        pub f_fsid: ::fsid_t,
-        pub f_namelen: ::__fsword_t,
-        pub f_frsize: ::__fsword_t,
-        pub f_flags: ::__fsword_t,
-        pub f_spare: [::__fsword_t; 4],
+        __pad3: i32,
     }
 
     pub struct statvfs64 {
