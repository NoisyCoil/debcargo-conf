Source: rust-backdown
Section: utils
Priority: optional
Build-Depends: debhelper-compat (= 13),
 dh-sequence-cargo,
 cargo:native,
 rustc:native (>= 1.59),
 libstd-rust-dev,
 librust-anyhow-1-dev (>= 1.0.49-~~),
 librust-argh-0.1-dev (>= 0.1.4-~~),
 librust-blake3-1-dev (>= 1.4-~~),
 librust-chrono-0.4-dev,
 librust-cli-log-2-dev,
 librust-crossbeam-0.8-dev,
 librust-file-size-1-dev,
 librust-fnv-1-dev (>= 1.0.7-~~),
 librust-lazy-regex-2-dev,
 librust-phf-0.11+macros-dev,
 librust-rayon-1-dev (>= 1.3-~~),
 librust-serde-1-dev,
 librust-serde-json-1-dev,
 librust-termimad-0.29-dev
Maintainer: Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
Uploaders:
 Blair Noctis <ncts@debian.org>
Standards-Version: 4.7.0
Vcs-Git: https://salsa.debian.org/rust-team/debcargo-conf.git [src/backdown]
Vcs-Browser: https://salsa.debian.org/rust-team/debcargo-conf/tree/master/src/backdown
X-Cargo-Crate: backdown
Rules-Requires-Root: no

Package: backdown
Architecture: any
Multi-Arch: allowed
Section: utils
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
 ${cargo:Depends}
Built-Using: ${cargo:Built-Using}
X-Cargo-Built-Using: ${cargo:X-Cargo-Built-Using}
Description: helps you safely and ergonomically remove duplicate files
 Based on author's observation of frequent patterns of duplicate build-up with
 time, especially images and other media files, backdown asks some questions
 about the duplicates in a way the user can easily decide which to keep. The
 list is staged until the end for the user to confirm removal, or it can be
 optionally exported as JSON for further inspection. It also has the option to
 replace files to be removed with links pointing to the kept ones.
